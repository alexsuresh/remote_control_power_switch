For aesthetic viewing refer 
Original article @ http://alexndis.wix.com/home

**Operate a remote power switch using various wireless technologies
**

Background

Every time I wanted to refill my domestic overhead water tank, I had to run downstairs to switch ON/OFF the 220V power supply to a jet pump motor connected to my bore well. Well, I could always rewire so that the 220V power switch is shifted upstairs. But I wanted to wirelessly control this motor switch from upstairs itself.

Introduction

The goal was to scavenge and reuse the parts in my hobby electronics stash and build a solution. Don’t buy a single part. Fortunately I had enough parts/boards to come up with 3 different solutions mentioned below, each with its own merits and pitfalls

Solutions

For discussion sake, we split solution(s) into a common base part and the 3 different types of wireless connectivity I used.

Common base part

This part  is located downstairs near the mechanical switch board. One may also refer to the photos section. I did not want to disturb the original mechanical motor switch based wiring. So, I connected in parallel a DPDT ( SPDT would suffice, but remember, the goal is not to buy anything new ) switch/relay with a typical relay coil 5V supply and an input terminal of 3.3/5V to open or close the 220V power supply to the motor. The input terminal is powered by the digital GPIO pin output of whatever board I used ( out of three  mentioned below ) as the receiver.

Wireless solution 1

H/W used
Bluetooth BLE device as receiver. Used this board http://www.cypress.com/documentation/development-kitsboards/cy8ckit-042-ble-bluetooth-low-energy-ble-pioneer-kit
Any Bluetooth smart ready ( just a fancy marketing name for BLE protocol) android device with version 4.4 & later as sender/transmitter

S/W developed/used

Firmware for BLE board. IDE/Compiler is “PSOC creator” from Cypress
Custom Android application developed using android studio IDE. 
Source code and description can be downloaded here

Board(s) setup and method of operation

1. Refer to “common base part” above. Connect the GPIO p3[0] which is pin# 1 in J2 I/O header to the relay switch plus get any GROUND line from board to relay as shown in the photo. Power everything by USB
2. Install APK in any android device and launch application
3. Android application has a simple switch control widget. Refer photo. Push/swipe to switch on/off motor. 

Notes
Uses custom profile. Android device as the GATT client playing the Master/Central GAP role, connectable undirected advertiser. Cypress PSOC/PROC BLE board acts as GATT server, playing Slave/Peripheral GAP role, and passive-scanner



Wireless Solution type 2 (currently using, preferred)

H/W used

TI’s eZ430-Chronos-433 kit. http://www.ti.com/tool/ez430-chronos#descriptionArea
Wrist watch in the kit as the remote control, sender device.
Chronos AP USB dongle in the kit as the receiver. Soldering needed to take out a GPIO line and ground. Change firmware in the white-pcb chronos AP usb dongle. Drive GPIO to switch on/off motor

S/W developed/used

Ported TI’s simpliciTI network stack on to the watch h/w
change firmware in the white-pcb chronos AP usb dongle. Drive GPIO to switch on/off motor

Board(s) setup and method of operation

1. Refer to “common base part” above. Connect the GPIO PJ.0/TDO which is test point 2 ( soldered ) in the side of the board to the relay switch plus get any GROUND line from board to relay as shown in the photo. Power everything by USB
2. Press the watch buttons to switch on/off motor. Push the <DOWN> button in the watch to establish a wireless Link. Press <UP> button to switch ON motor. Push the <DOWN> button again to DE-link to save watch battery power. Press <#> to switch OFF motor

Solution type 3

More or less similar to type-2 above, but used a set of eZ430-RF2500 boards instead of the watch kit.

H/W used

http://www.ti.com/tool/ez430-rf2500
One board, the receiver side is connected to the "base part" and the other board(sender) is attached to the battery holder provided in the eZ430-RF2500 kit.

S/W used/ported

Modified SimpliciTI-1.1.1-WSN430 firmware available on the internet both for the transmitter and the receiver.

Board(s) setup and method of operation

1. Refer to “common base part” above. Connect the <....TO DO...> to the relay switch plus get any GROUND line from board to relay as shown in the photo. Power everything by USB
2. Press the one and only USER switch in the battery-holder side red board to switch ON/OFF motor

Conclusion

Though a trivial project, one might learn a few things from actually doing this. I learnt,
In practical home hobby projects, using off-the-shelf boards available in market, only few feet ( say around 10-15 )  range is possible wirelessly, though chip vendors may claim big distances. Commercial designs may achieve this. Similar observation for battery power. A 3V button cell drained out in 15 days, even when networking sw/hw components were powered only on-demand. Again, commercial designs may be a different story, but I used reference designs and sample/example firmware
Antennae design and position matters
Wireless technology used really mattered for range. For e.g the 433 Mhz solution worked well, TI-propriteray ( some kind of gaussian QPSK ) 2.5 Ghz next, and using BT, provided inconsistent results for different Android phones.( something to do with scan/discovery mechanisms in this protocol/stack OR phone H/W like TX power, receiver sensitivity, antennae design/placement ?? )


Software description , more details

.......IN THE WORKS.....Come back later please......

for BT
1. write about android app design here…..
2. write about cypress psoc firmware , like set TX power, GPIO etc., here….

3. Uses minsdk >= 21 which means android 4.4&up. Uses lots of Android-Bluetooth BLE APIs/callbacks for scan/discovery, connection and read/write BLE profile/GATT characteristics.
4. Cypress firmware is build from scratch and _not_ from any example. Means, some proficiency required in the cypress PSOC creater IDE to understand the source code. But for a simple description
5. Define a customHandler for hooking into the BLE stack events which are notified asynchronously
6. Read/Write 0/1 to custom characteristics to switch ON/OFF motor
7. When the stack comes on, increase Tx power to+3dbM

Software points for TI boards
1. ported simpliciTI.
1. Ez430-rf2500 kits used wsn sample code. Easiest to do.  Only minor challenge move the build environment from code composer studio to GCC/toolchain environment
2. chronos watch firmware– tried openchronos-ng initially, but simpliciTI N/W stack not supported. So modified/simplified existing watch firmware that came with the kit, to simply send PPT mode up/down button presses for motor on/off commands. 
3. Chronos/receiver AP fw – always on USB power, listening for link requests every few seconds and receive data