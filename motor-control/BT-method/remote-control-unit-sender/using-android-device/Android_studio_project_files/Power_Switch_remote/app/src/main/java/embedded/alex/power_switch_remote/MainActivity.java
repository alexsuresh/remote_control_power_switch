package embedded.alex.power_switch_remote;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TargetApi(21)
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Alex Activity" ;

    private BluetoothAdapter mBluetoothAdapter;
    private int REQUEST_ENABLE_BT = 1;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 20000; // 20 seconds
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothGatt mGatt;
    private byte mOnRoffmotorSwitch=0;

    private TextView mTextViewStatusMessages ;
    private Switch mSwitchRemote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextViewStatusMessages = (TextView) findViewById(R.id.textViewStatusMessages) ;
        mSwitchRemote = (Switch)findViewById(R.id.switch1);

        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported",
                    Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();


        mSwitchRemote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mOnRoffmotorSwitch = (isChecked) ? (byte) 1 : (byte) 0;

                writeCustomCharacteristic();

                if (mOnRoffmotorSwitch == 1)
                    mTextViewStatusMessages.setText("Motor currently ON");
                else
                    mTextViewStatusMessages.setText("Motor currently OFF");
            }
        });

    }

    @Override
    protected void onResume() {
        String s = getString(R.string.service_name_remote_switch);

        super.onResume();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {

            mTextViewStatusMessages.setText("Searching for nearby power switches...");

            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
                ScanFilter sf = new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(s))
                        .build();
                filters.add(sf);
            }
            scanLeDevice(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            scanLeDevice(false);
        }
    }

    @Override
    protected void onDestroy() {
        if (mGatt != null) {
            mGatt.disconnect();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mGatt.close();
            mGatt = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT < 21) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    } else {
                        mLEScanner.stopScan(mScanCallback);

                    }
                }
            }, SCAN_PERIOD);
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                mLEScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                // alex mLEScanner.stopScan(mScanCallback);
            }
        }
    }


    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            mTextViewStatusMessages.setTextColor(Color.BLUE);
            mTextViewStatusMessages.setText("Remote Switch found");
            Log.i("callbackType", String.valueOf(callbackType));
            Log.i("result", result.toString());
            BluetoothDevice btDevice = result.getDevice();

            connectToDevice(btDevice);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
            mTextViewStatusMessages.setTextColor(Color.RED);
            mTextViewStatusMessages.setText("NO remote switches found");

        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("onLeScan", device.toString());
                            connectToDevice(device);
                        }
                    });
                }
            };

    public void connectToDevice(BluetoothDevice device) {
        if (mGatt == null) {
            mGatt = device.connectGatt(this, false, gattCallback);
            // alex scanLeDevice(false);// will stop after first device detection
        }
    }

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i("gattCallback", "STATE_CONNECTED");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallback", "STATE_DISCONNECTED");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mTextViewStatusMessages.setTextColor(Color.RED);
                            mTextViewStatusMessages.setText("BT connection LOST");
                        }
                    });

                    break;
                default:
                    Log.e("gattCallback", "STATE_OTHER");
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();
            Log.i("onServicesDiscovered", services.toString());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTextViewStatusMessages.setText("service discovery DONE. Trying to read switch status");
                }
            });

            readCustomCharacteristic();

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {
            Log.i("onCharacteristicRead", characteristic.toString());

            byte[] ba = characteristic.getValue();
            mOnRoffmotorSwitch = ba[0];


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    boolean b;
                    mSwitchRemote.setVisibility(View.VISIBLE);
                    b = mOnRoffmotorSwitch == 1 ? true : false;
                    mSwitchRemote.setChecked(b);
                    if (mOnRoffmotorSwitch == 1)
                        mTextViewStatusMessages.setText("Motor currently ON");
                    else
                        mTextViewStatusMessages.setText("Motor currently OFF");
                }
            });

            //gatt.disconnect();
        }
    };

    public void readCustomCharacteristic() {
        String s;

        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        s = getString(R.string.service_name_remote_switch);

        BluetoothGattService mCustomService;
        mCustomService = mGatt.getService(UUID.fromString(s));
        if(mCustomService == null){
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the READ characteristic from the service*/
        s = getString(R.string.characteristics_name_device_type_motor);

        BluetoothGattCharacteristic mReadCharacteristic ;
        mReadCharacteristic = mCustomService.getCharacteristic(UUID.fromString(s));

        if(mGatt.readCharacteristic(mReadCharacteristic) == false){
            Log.w(TAG, "Failed to start BHEL avenue motor");
        }
    }

    public void writeCustomCharacteristic() {
        String s;

        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        s = getString(R.string.service_name_remote_switch);

        BluetoothGattService mCustomService;
        mCustomService = mGatt.getService(UUID.fromString(s));
        if(mCustomService == null){
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the WRITE characteristic from the service*/
        s = getString(R.string.characteristics_name_device_type_motor);

        BluetoothGattCharacteristic mWriteCharacteristic ;
        mWriteCharacteristic = mCustomService.getCharacteristic(UUID.fromString(s));
        mWriteCharacteristic.setValue(mOnRoffmotorSwitch, android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        if(mGatt.writeCharacteristic(mWriteCharacteristic) == false){
            Log.w(TAG, "Failed to start BHEL avenue motor");
        }
    }
}
