/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>

void GeneralEventHandler(uint32 event, void * eventParam)
{
	/* Structure to store data written by Client */	
	CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;
    uint8 motorState = 0 ; // OFF	
    CYBLE_GATT_HANDLE_VALUE_PAIR_T		motorHandle;
    CYBLE_BLESS_PWR_IN_DB_T txPower;
    
	switch(event)
    {
		case CYBLE_EVT_STACK_ON:
        
            txPower.bleSsChId = CYBLE_LL_ADV_CH_TYPE; 
            CyBle_GetTxPowerLevel(&txPower);
            txPower.blePwrLevelInDbm = CYBLE_LL_PWR_LVL_MAX;
            CyBle_SetTxPowerLevel(&txPower);
            
            txPower.bleSsChId = CYBLE_LL_CONN_CH_TYPE ;
            CyBle_GetTxPowerLevel(&txPower);
            txPower.blePwrLevelInDbm = CYBLE_LL_PWR_LVL_MAX;
            CyBle_SetTxPowerLevel(&txPower);
             
            
			/* BLE stack is on. Start BLE advertisement */
			CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
		break;
				
		case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
			/* This event is generated at GAP disconnection. */

			
			/* Switch off MOTOR TODO alex */
			
			
						
			/* Restart advertisement */
			CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
		break;

        case CYBLE_EVT_GATTS_WRITE_REQ: 
			/* This event is generated when the connected Central device sends a
			* Write request. The parameter contains the data written */
			
			/* Extract the Write data sent by Client */
            wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T *) eventParam;
			
			/* If the attribute handle of the characteristic written to 
			* is equal to that of RGB_LED characteristic, then extract 
			* the RGB LED data */
			if(CYBLE_MOTOR_CONTROL_ONROFF_CHAR_HANDLE == wrReqParam->handleValPair.attrHandle)
            {
				/* Store RGB LED data in local array */
                motorState = wrReqParam->handleValPair.value.val[0];
               
				/* Update the PrISM component density value to represent color */
                motor_switch_Write(motorState);
				
				/* Update the GATT DB for RGB LED read characteristics*/
				
            	/* Update the RGB LED attribute value. This will allow 
            	* Client device to read the existing color values over 
            	* RGB LED characteristic */
            	motorHandle.attrHandle = CYBLE_MOTOR_CONTROL_ONROFF_CHAR_HANDLE;
                motorHandle.value.val = wrReqParam->handleValPair.value.val ;
                motorHandle.value.len = 1 ;
                
                CyBle_GattsWriteAttributeValue(&motorHandle, 
            									0, 
            									&cyBle_connHandle, 
            									CYBLE_GATT_DB_PEER_INITIATED);
    
            }
			
			/* Send the response to the write request. */
			CyBle_GattsWriteRsp(cyBle_connHandle);
			break;

        default:

       	break;
    }   	
}

int main()
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    
    CyBle_Start(GeneralEventHandler);

    for(;;)
    {
        /* Place your application code here. */
        CyBle_ProcessEvents();
    }
}

/* [] END OF FILE */
