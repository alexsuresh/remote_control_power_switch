/*******************************************************************************
* File Name: alex_BLE_bless_isr.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_alex_BLE_bless_isr_H)
#define CY_ISR_alex_BLE_bless_isr_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void alex_BLE_bless_isr_Start(void);
void alex_BLE_bless_isr_StartEx(cyisraddress address);
void alex_BLE_bless_isr_Stop(void);

CY_ISR_PROTO(alex_BLE_bless_isr_Interrupt);

void alex_BLE_bless_isr_SetVector(cyisraddress address);
cyisraddress alex_BLE_bless_isr_GetVector(void);

void alex_BLE_bless_isr_SetPriority(uint8 priority);
uint8 alex_BLE_bless_isr_GetPriority(void);

void alex_BLE_bless_isr_Enable(void);
uint8 alex_BLE_bless_isr_GetState(void);
void alex_BLE_bless_isr_Disable(void);

void alex_BLE_bless_isr_SetPending(void);
void alex_BLE_bless_isr_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the alex_BLE_bless_isr ISR. */
#define alex_BLE_bless_isr_INTC_VECTOR            ((reg32 *) alex_BLE_bless_isr__INTC_VECT)

/* Address of the alex_BLE_bless_isr ISR priority. */
#define alex_BLE_bless_isr_INTC_PRIOR             ((reg32 *) alex_BLE_bless_isr__INTC_PRIOR_REG)

/* Priority of the alex_BLE_bless_isr interrupt. */
#define alex_BLE_bless_isr_INTC_PRIOR_NUMBER      alex_BLE_bless_isr__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable alex_BLE_bless_isr interrupt. */
#define alex_BLE_bless_isr_INTC_SET_EN            ((reg32 *) alex_BLE_bless_isr__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the alex_BLE_bless_isr interrupt. */
#define alex_BLE_bless_isr_INTC_CLR_EN            ((reg32 *) alex_BLE_bless_isr__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the alex_BLE_bless_isr interrupt state to pending. */
#define alex_BLE_bless_isr_INTC_SET_PD            ((reg32 *) alex_BLE_bless_isr__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the alex_BLE_bless_isr interrupt. */
#define alex_BLE_bless_isr_INTC_CLR_PD            ((reg32 *) alex_BLE_bless_isr__INTC_CLR_PD_REG)



#endif /* CY_ISR_alex_BLE_bless_isr_H */


/* [] END OF FILE */
