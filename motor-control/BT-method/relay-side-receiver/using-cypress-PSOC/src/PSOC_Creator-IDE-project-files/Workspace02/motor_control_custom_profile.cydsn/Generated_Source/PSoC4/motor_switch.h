/*******************************************************************************
* File Name: motor_switch.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_motor_switch_H) /* Pins motor_switch_H */
#define CY_PINS_motor_switch_H

#include "cytypes.h"
#include "cyfitter.h"
#include "motor_switch_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} motor_switch_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   motor_switch_Read(void);
void    motor_switch_Write(uint8 value);
uint8   motor_switch_ReadDataReg(void);
#if defined(motor_switch__PC) || (CY_PSOC4_4200L) 
    void    motor_switch_SetDriveMode(uint8 mode);
#endif
void    motor_switch_SetInterruptMode(uint16 position, uint16 mode);
uint8   motor_switch_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void motor_switch_Sleep(void); 
void motor_switch_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(motor_switch__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define motor_switch_DRIVE_MODE_BITS        (3)
    #define motor_switch_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - motor_switch_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the motor_switch_SetDriveMode() function.
         *  @{
         */
        #define motor_switch_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define motor_switch_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define motor_switch_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define motor_switch_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define motor_switch_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define motor_switch_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define motor_switch_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define motor_switch_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define motor_switch_MASK               motor_switch__MASK
#define motor_switch_SHIFT              motor_switch__SHIFT
#define motor_switch_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in motor_switch_SetInterruptMode() function.
     *  @{
     */
        #define motor_switch_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define motor_switch_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define motor_switch_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define motor_switch_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(motor_switch__SIO)
    #define motor_switch_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(motor_switch__PC) && (CY_PSOC4_4200L)
    #define motor_switch_USBIO_ENABLE               ((uint32)0x80000000u)
    #define motor_switch_USBIO_DISABLE              ((uint32)(~motor_switch_USBIO_ENABLE))
    #define motor_switch_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define motor_switch_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define motor_switch_USBIO_ENTER_SLEEP          ((uint32)((1u << motor_switch_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << motor_switch_USBIO_SUSPEND_DEL_SHIFT)))
    #define motor_switch_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << motor_switch_USBIO_SUSPEND_SHIFT)))
    #define motor_switch_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << motor_switch_USBIO_SUSPEND_DEL_SHIFT)))
    #define motor_switch_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(motor_switch__PC)
    /* Port Configuration */
    #define motor_switch_PC                 (* (reg32 *) motor_switch__PC)
#endif
/* Pin State */
#define motor_switch_PS                     (* (reg32 *) motor_switch__PS)
/* Data Register */
#define motor_switch_DR                     (* (reg32 *) motor_switch__DR)
/* Input Buffer Disable Override */
#define motor_switch_INP_DIS                (* (reg32 *) motor_switch__PC2)

/* Interrupt configuration Registers */
#define motor_switch_INTCFG                 (* (reg32 *) motor_switch__INTCFG)
#define motor_switch_INTSTAT                (* (reg32 *) motor_switch__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define motor_switch_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(motor_switch__SIO)
    #define motor_switch_SIO_REG            (* (reg32 *) motor_switch__SIO)
#endif /* (motor_switch__SIO_CFG) */

/* USBIO registers */
#if !defined(motor_switch__PC) && (CY_PSOC4_4200L)
    #define motor_switch_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define motor_switch_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define motor_switch_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define motor_switch_DRIVE_MODE_SHIFT       (0x00u)
#define motor_switch_DRIVE_MODE_MASK        (0x07u << motor_switch_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins motor_switch_H */


/* [] END OF FILE */
