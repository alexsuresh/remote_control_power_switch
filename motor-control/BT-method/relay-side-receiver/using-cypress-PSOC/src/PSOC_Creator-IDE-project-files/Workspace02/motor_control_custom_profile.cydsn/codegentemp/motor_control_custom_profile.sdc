# THIS FILE IS AUTOMATICALLY GENERATED
# Project: C:\Users\a\Documents\psoc_alex\Workspace02\motor_control_custom_profile.cydsn\motor_control_custom_profile.cyprj
# Date: Mon, 25 Apr 2016 12:20:51 GMT
#set_units -time ns
create_clock -name {CyRouted1} -period 20.833333333333332 -waveform {0 10.4166666666667} [list [get_pins {ClockBlock/dsi_in_0}]]
create_clock -name {CyWCO} -period 30517.578125 -waveform {0 15258.7890625} [list [get_pins {ClockBlock/wco}]]
create_clock -name {CyLFCLK} -period 30517.578125 -waveform {0 15258.7890625} [list [get_pins {ClockBlock/lfclk}]]
create_clock -name {CyILO} -period 31250 -waveform {0 15625} [list [get_pins {ClockBlock/ilo}]]
create_clock -name {CyECO} -period 41.666666666666664 -waveform {0 20.8333333333333} [list [get_pins {ClockBlock/eco}]]
create_clock -name {CyIMO} -period 20.833333333333332 -waveform {0 10.4166666666667} [list [get_pins {ClockBlock/imo}]]
create_clock -name {CyHFCLK} -period 20.833333333333332 -waveform {0 10.4166666666667} [list [get_pins {ClockBlock/hfclk}]]
create_clock -name {CySYSCLK} -period 20.833333333333332 -waveform {0 10.4166666666667} [list [get_pins {ClockBlock/sysclk}]]


# Component constraints for C:\Users\a\Documents\psoc_alex\Workspace02\motor_control_custom_profile.cydsn\TopDesign\TopDesign.cysch
# Project: C:\Users\a\Documents\psoc_alex\Workspace02\motor_control_custom_profile.cydsn\motor_control_custom_profile.cyprj
# Date: Mon, 25 Apr 2016 12:20:44 GMT
