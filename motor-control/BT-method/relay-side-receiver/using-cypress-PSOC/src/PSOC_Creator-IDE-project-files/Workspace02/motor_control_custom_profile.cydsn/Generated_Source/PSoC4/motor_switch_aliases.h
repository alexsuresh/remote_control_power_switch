/*******************************************************************************
* File Name: motor_switch.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_motor_switch_ALIASES_H) /* Pins motor_switch_ALIASES_H */
#define CY_PINS_motor_switch_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define motor_switch_0			(motor_switch__0__PC)
#define motor_switch_0_PS		(motor_switch__0__PS)
#define motor_switch_0_PC		(motor_switch__0__PC)
#define motor_switch_0_DR		(motor_switch__0__DR)
#define motor_switch_0_SHIFT	(motor_switch__0__SHIFT)
#define motor_switch_0_INTR	((uint16)((uint16)0x0003u << (motor_switch__0__SHIFT*2u)))

#define motor_switch_INTR_ALL	 ((uint16)(motor_switch_0_INTR))


#endif /* End Pins motor_switch_ALIASES_H */


/* [] END OF FILE */
