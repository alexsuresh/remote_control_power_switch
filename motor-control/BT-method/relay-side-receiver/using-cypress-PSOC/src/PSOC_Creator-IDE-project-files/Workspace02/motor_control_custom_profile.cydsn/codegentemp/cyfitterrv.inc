    IF :LNOT::DEF:INCLUDED_CYFITTERRV_INC
INCLUDED_CYFITTERRV_INC EQU 1
    GET cydevicerv_trm.inc

; alex_BLE_bless_isr
alex_BLE_bless_isr__INTC_CLR_EN_REG EQU CYREG_CM0_ICER
alex_BLE_bless_isr__INTC_CLR_PD_REG EQU CYREG_CM0_ICPR
alex_BLE_bless_isr__INTC_MASK EQU 0x1000
alex_BLE_bless_isr__INTC_NUMBER EQU 12
alex_BLE_bless_isr__INTC_PRIOR_MASK EQU 0xC0
alex_BLE_bless_isr__INTC_PRIOR_NUM EQU 0
alex_BLE_bless_isr__INTC_PRIOR_REG EQU CYREG_CM0_IPR3
alex_BLE_bless_isr__INTC_SET_EN_REG EQU CYREG_CM0_ISER
alex_BLE_bless_isr__INTC_SET_PD_REG EQU CYREG_CM0_ISPR

; alex_BLE_cy_m0s8_ble
alex_BLE_cy_m0s8_ble__ADC_BUMP1 EQU CYREG_BLE_BLERD_ADC_BUMP1
alex_BLE_cy_m0s8_ble__ADC_BUMP2 EQU CYREG_BLE_BLERD_ADC_BUMP2
alex_BLE_cy_m0s8_ble__ADV_CH_TX_POWER EQU CYREG_BLE_BLELL_ADV_CH_TX_POWER
alex_BLE_cy_m0s8_ble__ADV_CONFIG EQU CYREG_BLE_BLELL_ADV_CONFIG
alex_BLE_cy_m0s8_ble__ADV_INTERVAL_TIMEOUT EQU CYREG_BLE_BLELL_ADV_INTERVAL_TIMEOUT
alex_BLE_cy_m0s8_ble__ADV_INTR EQU CYREG_BLE_BLELL_ADV_INTR
alex_BLE_cy_m0s8_ble__ADV_NEXT_INSTANT EQU CYREG_BLE_BLELL_ADV_NEXT_INSTANT
alex_BLE_cy_m0s8_ble__ADV_PARAMS EQU CYREG_BLE_BLELL_ADV_PARAMS
alex_BLE_cy_m0s8_ble__ADV_SCN_RSP_TX_FIFO EQU CYREG_BLE_BLELL_ADV_SCN_RSP_TX_FIFO
alex_BLE_cy_m0s8_ble__ADV_TX_DATA_FIFO EQU CYREG_BLE_BLELL_ADV_TX_DATA_FIFO
alex_BLE_cy_m0s8_ble__AGC EQU CYREG_BLE_BLERD_AGC
alex_BLE_cy_m0s8_ble__BALUN EQU CYREG_BLE_BLERD_BALUN
alex_BLE_cy_m0s8_ble__BB_BUMP1 EQU CYREG_BLE_BLERD_BB_BUMP1
alex_BLE_cy_m0s8_ble__BB_BUMP2 EQU CYREG_BLE_BLERD_BB_BUMP2
alex_BLE_cy_m0s8_ble__BB_XO EQU CYREG_BLE_BLERD_BB_XO
alex_BLE_cy_m0s8_ble__BB_XO_CAPTRIM EQU CYREG_BLE_BLERD_BB_XO_CAPTRIM
alex_BLE_cy_m0s8_ble__CE_CNFG_STS_REGISTER EQU CYREG_BLE_BLELL_CE_CNFG_STS_REGISTER
alex_BLE_cy_m0s8_ble__CE_LENGTH EQU CYREG_BLE_BLELL_CE_LENGTH
alex_BLE_cy_m0s8_ble__CFG_1_FCAL EQU CYREG_BLE_BLERD_CFG_1_FCAL
alex_BLE_cy_m0s8_ble__CFG_2_FCAL EQU CYREG_BLE_BLERD_CFG_2_FCAL
alex_BLE_cy_m0s8_ble__CFG_3_FCAL EQU CYREG_BLE_BLERD_CFG_3_FCAL
alex_BLE_cy_m0s8_ble__CFG_4_FCAL EQU CYREG_BLE_BLERD_CFG_4_FCAL
alex_BLE_cy_m0s8_ble__CFG_5_FCAL EQU CYREG_BLE_BLERD_CFG_5_FCAL
alex_BLE_cy_m0s8_ble__CFG_6_FCAL EQU CYREG_BLE_BLERD_CFG_6_FCAL
alex_BLE_cy_m0s8_ble__CFG1 EQU CYREG_BLE_BLERD_CFG1
alex_BLE_cy_m0s8_ble__CFG2 EQU CYREG_BLE_BLERD_CFG2
alex_BLE_cy_m0s8_ble__CFGCTRL EQU CYREG_BLE_BLERD_CFGCTRL
alex_BLE_cy_m0s8_ble__CLOCK_CONFIG EQU CYREG_BLE_BLELL_CLOCK_CONFIG
alex_BLE_cy_m0s8_ble__COMMAND_REGISTER EQU CYREG_BLE_BLELL_COMMAND_REGISTER
alex_BLE_cy_m0s8_ble__CONN_CE_COUNTER EQU CYREG_BLE_BLELL_CONN_CE_COUNTER
alex_BLE_cy_m0s8_ble__CONN_CE_INSTANT EQU CYREG_BLE_BLELL_CONN_CE_INSTANT
alex_BLE_cy_m0s8_ble__CONN_CH_TX_POWER EQU CYREG_BLE_BLELL_CONN_CH_TX_POWER
alex_BLE_cy_m0s8_ble__CONN_CONFIG EQU CYREG_BLE_BLELL_CONN_CONFIG
alex_BLE_cy_m0s8_ble__CONN_INDEX EQU CYREG_BLE_BLELL_CONN_INDEX
alex_BLE_cy_m0s8_ble__CONN_INTERVAL EQU CYREG_BLE_BLELL_CONN_INTERVAL
alex_BLE_cy_m0s8_ble__CONN_INTR EQU CYREG_BLE_BLELL_CONN_INTR
alex_BLE_cy_m0s8_ble__CONN_INTR_MASK EQU CYREG_BLE_BLELL_CONN_INTR_MASK
alex_BLE_cy_m0s8_ble__CONN_PARAM1 EQU CYREG_BLE_BLELL_CONN_PARAM1
alex_BLE_cy_m0s8_ble__CONN_PARAM2 EQU CYREG_BLE_BLELL_CONN_PARAM2
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD0 EQU CYREG_BLE_BLELL_CONN_REQ_WORD0
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD1 EQU CYREG_BLE_BLELL_CONN_REQ_WORD1
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD10 EQU CYREG_BLE_BLELL_CONN_REQ_WORD10
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD11 EQU CYREG_BLE_BLELL_CONN_REQ_WORD11
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD2 EQU CYREG_BLE_BLELL_CONN_REQ_WORD2
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD3 EQU CYREG_BLE_BLELL_CONN_REQ_WORD3
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD4 EQU CYREG_BLE_BLELL_CONN_REQ_WORD4
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD5 EQU CYREG_BLE_BLELL_CONN_REQ_WORD5
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD6 EQU CYREG_BLE_BLELL_CONN_REQ_WORD6
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD7 EQU CYREG_BLE_BLELL_CONN_REQ_WORD7
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD8 EQU CYREG_BLE_BLELL_CONN_REQ_WORD8
alex_BLE_cy_m0s8_ble__CONN_REQ_WORD9 EQU CYREG_BLE_BLELL_CONN_REQ_WORD9
alex_BLE_cy_m0s8_ble__CONN_RXMEM_BASE_ADDR EQU CYREG_BLE_BLELL_CONN_RXMEM_BASE_ADDR
alex_BLE_cy_m0s8_ble__CONN_STATUS EQU CYREG_BLE_BLELL_CONN_STATUS
alex_BLE_cy_m0s8_ble__CONN_TXMEM_BASE_ADDR EQU CYREG_BLE_BLELL_CONN_TXMEM_BASE_ADDR
alex_BLE_cy_m0s8_ble__CONN_UPDATE_NEW_INTERVAL EQU CYREG_BLE_BLELL_CONN_UPDATE_NEW_INTERVAL
alex_BLE_cy_m0s8_ble__CONN_UPDATE_NEW_LATENCY EQU CYREG_BLE_BLELL_CONN_UPDATE_NEW_LATENCY
alex_BLE_cy_m0s8_ble__CONN_UPDATE_NEW_SL_INTERVAL EQU CYREG_BLE_BLELL_CONN_UPDATE_NEW_SL_INTERVAL
alex_BLE_cy_m0s8_ble__CONN_UPDATE_NEW_SUP_TO EQU CYREG_BLE_BLELL_CONN_UPDATE_NEW_SUP_TO
alex_BLE_cy_m0s8_ble__CTR1 EQU CYREG_BLE_BLERD_CTR1
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_H0 EQU CYREG_BLE_BLELL_DATA_CHANNELS_H0
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_H1 EQU CYREG_BLE_BLELL_DATA_CHANNELS_H1
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_L0 EQU CYREG_BLE_BLELL_DATA_CHANNELS_L0
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_L1 EQU CYREG_BLE_BLELL_DATA_CHANNELS_L1
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_M0 EQU CYREG_BLE_BLELL_DATA_CHANNELS_M0
alex_BLE_cy_m0s8_ble__DATA_CHANNELS_M1 EQU CYREG_BLE_BLELL_DATA_CHANNELS_M1
alex_BLE_cy_m0s8_ble__DATA_LIST_ACK_UPDATE__STATUS EQU CYREG_BLE_BLELL_DATA_LIST_ACK_UPDATE__STATUS
alex_BLE_cy_m0s8_ble__DATA_LIST_SENT_UPDATE__STATUS EQU CYREG_BLE_BLELL_DATA_LIST_SENT_UPDATE__STATUS
alex_BLE_cy_m0s8_ble__DATA_MEM_DESCRIPTOR0 EQU CYREG_BLE_BLELL_DATA_MEM_DESCRIPTOR0
alex_BLE_cy_m0s8_ble__DATA_MEM_DESCRIPTOR1 EQU CYREG_BLE_BLELL_DATA_MEM_DESCRIPTOR1
alex_BLE_cy_m0s8_ble__DATA_MEM_DESCRIPTOR2 EQU CYREG_BLE_BLELL_DATA_MEM_DESCRIPTOR2
alex_BLE_cy_m0s8_ble__DATA_MEM_DESCRIPTOR3 EQU CYREG_BLE_BLELL_DATA_MEM_DESCRIPTOR3
alex_BLE_cy_m0s8_ble__DATA_MEM_DESCRIPTOR4 EQU CYREG_BLE_BLELL_DATA_MEM_DESCRIPTOR4
alex_BLE_cy_m0s8_ble__DATA0 EQU CYREG_BLE_BLELL_DATA0
alex_BLE_cy_m0s8_ble__DATA1 EQU CYREG_BLE_BLELL_DATA1
alex_BLE_cy_m0s8_ble__DATA10 EQU CYREG_BLE_BLELL_DATA10
alex_BLE_cy_m0s8_ble__DATA11 EQU CYREG_BLE_BLELL_DATA11
alex_BLE_cy_m0s8_ble__DATA12 EQU CYREG_BLE_BLELL_DATA12
alex_BLE_cy_m0s8_ble__DATA13 EQU CYREG_BLE_BLELL_DATA13
alex_BLE_cy_m0s8_ble__DATA2 EQU CYREG_BLE_BLELL_DATA2
alex_BLE_cy_m0s8_ble__DATA3 EQU CYREG_BLE_BLELL_DATA3
alex_BLE_cy_m0s8_ble__DATA4 EQU CYREG_BLE_BLELL_DATA4
alex_BLE_cy_m0s8_ble__DATA5 EQU CYREG_BLE_BLELL_DATA5
alex_BLE_cy_m0s8_ble__DATA6 EQU CYREG_BLE_BLELL_DATA6
alex_BLE_cy_m0s8_ble__DATA7 EQU CYREG_BLE_BLELL_DATA7
alex_BLE_cy_m0s8_ble__DATA8 EQU CYREG_BLE_BLELL_DATA8
alex_BLE_cy_m0s8_ble__DATA9 EQU CYREG_BLE_BLELL_DATA9
alex_BLE_cy_m0s8_ble__DBG_1 EQU CYREG_BLE_BLERD_DBG_1
alex_BLE_cy_m0s8_ble__DBG_2 EQU CYREG_BLE_BLERD_DBG_2
alex_BLE_cy_m0s8_ble__DBG_3 EQU CYREG_BLE_BLERD_DBG_3
alex_BLE_cy_m0s8_ble__DBG_BB EQU CYREG_BLE_BLERD_DBG_BB
alex_BLE_cy_m0s8_ble__DBUS EQU CYREG_BLE_BLERD_DBUS
alex_BLE_cy_m0s8_ble__DC EQU CYREG_BLE_BLERD_DC
alex_BLE_cy_m0s8_ble__DCCAL EQU CYREG_BLE_BLERD_DCCAL
alex_BLE_cy_m0s8_ble__DEV_PUB_ADDR_H EQU CYREG_BLE_BLELL_DEV_PUB_ADDR_H
alex_BLE_cy_m0s8_ble__DEV_PUB_ADDR_L EQU CYREG_BLE_BLELL_DEV_PUB_ADDR_L
alex_BLE_cy_m0s8_ble__DEV_PUB_ADDR_M EQU CYREG_BLE_BLELL_DEV_PUB_ADDR_M
alex_BLE_cy_m0s8_ble__DEVICE_RAND_ADDR_H EQU CYREG_BLE_BLELL_DEVICE_RAND_ADDR_H
alex_BLE_cy_m0s8_ble__DEVICE_RAND_ADDR_L EQU CYREG_BLE_BLELL_DEVICE_RAND_ADDR_L
alex_BLE_cy_m0s8_ble__DEVICE_RAND_ADDR_M EQU CYREG_BLE_BLELL_DEVICE_RAND_ADDR_M
alex_BLE_cy_m0s8_ble__DIAG1 EQU CYREG_BLE_BLERD_DIAG1
alex_BLE_cy_m0s8_ble__DPLL_CONFIG EQU CYREG_BLE_BLELL_DPLL_CONFIG
alex_BLE_cy_m0s8_ble__DSM1 EQU CYREG_BLE_BLERD_DSM1
alex_BLE_cy_m0s8_ble__DSM2 EQU CYREG_BLE_BLERD_DSM2
alex_BLE_cy_m0s8_ble__DSM3 EQU CYREG_BLE_BLERD_DSM3
alex_BLE_cy_m0s8_ble__DSM4 EQU CYREG_BLE_BLERD_DSM4
alex_BLE_cy_m0s8_ble__DSM5 EQU CYREG_BLE_BLERD_DSM5
alex_BLE_cy_m0s8_ble__DSM6 EQU CYREG_BLE_BLERD_DSM6
alex_BLE_cy_m0s8_ble__DTM_RX_PKT_COUNT EQU CYREG_BLE_BLELL_DTM_RX_PKT_COUNT
alex_BLE_cy_m0s8_ble__ENC_CONFIG EQU CYREG_BLE_BLELL_ENC_CONFIG
alex_BLE_cy_m0s8_ble__ENC_INTR EQU CYREG_BLE_BLELL_ENC_INTR
alex_BLE_cy_m0s8_ble__ENC_INTR_EN EQU CYREG_BLE_BLELL_ENC_INTR_EN
alex_BLE_cy_m0s8_ble__ENC_KEY0 EQU CYREG_BLE_BLELL_ENC_KEY0
alex_BLE_cy_m0s8_ble__ENC_KEY1 EQU CYREG_BLE_BLELL_ENC_KEY1
alex_BLE_cy_m0s8_ble__ENC_KEY2 EQU CYREG_BLE_BLELL_ENC_KEY2
alex_BLE_cy_m0s8_ble__ENC_KEY3 EQU CYREG_BLE_BLELL_ENC_KEY3
alex_BLE_cy_m0s8_ble__ENC_KEY4 EQU CYREG_BLE_BLELL_ENC_KEY4
alex_BLE_cy_m0s8_ble__ENC_KEY5 EQU CYREG_BLE_BLELL_ENC_KEY5
alex_BLE_cy_m0s8_ble__ENC_KEY6 EQU CYREG_BLE_BLELL_ENC_KEY6
alex_BLE_cy_m0s8_ble__ENC_KEY7 EQU CYREG_BLE_BLELL_ENC_KEY7
alex_BLE_cy_m0s8_ble__ENC_PARAMS EQU CYREG_BLE_BLELL_ENC_PARAMS
alex_BLE_cy_m0s8_ble__EVENT_ENABLE EQU CYREG_BLE_BLELL_EVENT_ENABLE
alex_BLE_cy_m0s8_ble__EVENT_INTR EQU CYREG_BLE_BLELL_EVENT_INTR
alex_BLE_cy_m0s8_ble__FCAL_TEST EQU CYREG_BLE_BLERD_FCAL_TEST
alex_BLE_cy_m0s8_ble__FPD_TEST EQU CYREG_BLE_BLERD_FPD_TEST
alex_BLE_cy_m0s8_ble__FSM EQU CYREG_BLE_BLERD_FSM
alex_BLE_cy_m0s8_ble__IM EQU CYREG_BLE_BLERD_IM
alex_BLE_cy_m0s8_ble__INIT_CONFIG EQU CYREG_BLE_BLELL_INIT_CONFIG
alex_BLE_cy_m0s8_ble__INIT_INTERVAL EQU CYREG_BLE_BLELL_INIT_INTERVAL
alex_BLE_cy_m0s8_ble__INIT_INTR EQU CYREG_BLE_BLELL_INIT_INTR
alex_BLE_cy_m0s8_ble__INIT_NEXT_INSTANT EQU CYREG_BLE_BLELL_INIT_NEXT_INSTANT
alex_BLE_cy_m0s8_ble__INIT_PARAM EQU CYREG_BLE_BLELL_INIT_PARAM
alex_BLE_cy_m0s8_ble__INIT_SCN_ADV_RX_FIFO EQU CYREG_BLE_BLELL_INIT_SCN_ADV_RX_FIFO
alex_BLE_cy_m0s8_ble__INIT_WINDOW EQU CYREG_BLE_BLELL_INIT_WINDOW
alex_BLE_cy_m0s8_ble__IQMIS EQU CYREG_BLE_BLERD_IQMIS
alex_BLE_cy_m0s8_ble__IV_MASTER0 EQU CYREG_BLE_BLELL_IV_MASTER0
alex_BLE_cy_m0s8_ble__IV_MASTER1 EQU CYREG_BLE_BLELL_IV_MASTER1
alex_BLE_cy_m0s8_ble__IV_SLAVE0 EQU CYREG_BLE_BLELL_IV_SLAVE0
alex_BLE_cy_m0s8_ble__IV_SLAVE1 EQU CYREG_BLE_BLELL_IV_SLAVE1
alex_BLE_cy_m0s8_ble__KVCAL EQU CYREG_BLE_BLERD_KVCAL
alex_BLE_cy_m0s8_ble__LDO EQU CYREG_BLE_BLERD_LDO
alex_BLE_cy_m0s8_ble__LDO_BYPASS EQU CYREG_BLE_BLERD_LDO_BYPASS
alex_BLE_cy_m0s8_ble__LE_PING_TIMER_ADDR EQU CYREG_BLE_BLELL_LE_PING_TIMER_ADDR
alex_BLE_cy_m0s8_ble__LE_PING_TIMER_NEXT_EXP EQU CYREG_BLE_BLELL_LE_PING_TIMER_NEXT_EXP
alex_BLE_cy_m0s8_ble__LE_PING_TIMER_OFFSET EQU CYREG_BLE_BLELL_LE_PING_TIMER_OFFSET
alex_BLE_cy_m0s8_ble__LE_PING_TIMER_WRAP_COUNT EQU CYREG_BLE_BLELL_LE_PING_TIMER_WRAP_COUNT
alex_BLE_cy_m0s8_ble__LE_RF_TEST_MODE EQU CYREG_BLE_BLELL_LE_RF_TEST_MODE
alex_BLE_cy_m0s8_ble__LF_CLK_CTRL EQU CYREG_BLE_BLESS_LF_CLK_CTRL
alex_BLE_cy_m0s8_ble__LL_CLK_EN EQU CYREG_BLE_BLESS_LL_CLK_EN
alex_BLE_cy_m0s8_ble__LL_DSM_CTRL EQU CYREG_BLE_BLESS_LL_DSM_CTRL
alex_BLE_cy_m0s8_ble__LL_DSM_INTR_STAT EQU CYREG_BLE_BLESS_LL_DSM_INTR_STAT
alex_BLE_cy_m0s8_ble__LLH_FEATURE_CONFIG EQU CYREG_BLE_BLELL_LLH_FEATURE_CONFIG
alex_BLE_cy_m0s8_ble__MIC_IN0 EQU CYREG_BLE_BLELL_MIC_IN0
alex_BLE_cy_m0s8_ble__MIC_IN1 EQU CYREG_BLE_BLELL_MIC_IN1
alex_BLE_cy_m0s8_ble__MIC_OUT0 EQU CYREG_BLE_BLELL_MIC_OUT0
alex_BLE_cy_m0s8_ble__MIC_OUT1 EQU CYREG_BLE_BLELL_MIC_OUT1
alex_BLE_cy_m0s8_ble__MODEM EQU CYREG_BLE_BLERD_MODEM
alex_BLE_cy_m0s8_ble__MONI EQU CYREG_BLE_BLERD_MONI
alex_BLE_cy_m0s8_ble__NEXT_CE_INSTANT EQU CYREG_BLE_BLELL_NEXT_CE_INSTANT
alex_BLE_cy_m0s8_ble__NEXT_RESP_TIMER_EXP EQU CYREG_BLE_BLELL_NEXT_RESP_TIMER_EXP
alex_BLE_cy_m0s8_ble__NEXT_SUP_TO EQU CYREG_BLE_BLELL_NEXT_SUP_TO
alex_BLE_cy_m0s8_ble__OFFSET_TO_FIRST_INSTANT EQU CYREG_BLE_BLELL_OFFSET_TO_FIRST_INSTANT
alex_BLE_cy_m0s8_ble__PACKET_COUNTER0 EQU CYREG_BLE_BLELL_PACKET_COUNTER0
alex_BLE_cy_m0s8_ble__PACKET_COUNTER1 EQU CYREG_BLE_BLELL_PACKET_COUNTER1
alex_BLE_cy_m0s8_ble__PACKET_COUNTER2 EQU CYREG_BLE_BLELL_PACKET_COUNTER2
alex_BLE_cy_m0s8_ble__PDU_ACCESS_ADDR_H_REGISTER EQU CYREG_BLE_BLELL_PDU_ACCESS_ADDR_H_REGISTER
alex_BLE_cy_m0s8_ble__PDU_ACCESS_ADDR_L_REGISTER EQU CYREG_BLE_BLELL_PDU_ACCESS_ADDR_L_REGISTER
alex_BLE_cy_m0s8_ble__PDU_RESP_TIMER EQU CYREG_BLE_BLELL_PDU_RESP_TIMER
alex_BLE_cy_m0s8_ble__PEER_ADDR_H EQU CYREG_BLE_BLELL_PEER_ADDR_H
alex_BLE_cy_m0s8_ble__PEER_ADDR_L EQU CYREG_BLE_BLELL_PEER_ADDR_L
alex_BLE_cy_m0s8_ble__PEER_ADDR_M EQU CYREG_BLE_BLELL_PEER_ADDR_M
alex_BLE_cy_m0s8_ble__POC_REG__TIM_CONTROL EQU CYREG_BLE_BLELL_POC_REG__TIM_CONTROL
alex_BLE_cy_m0s8_ble__RCCAL EQU CYREG_BLE_BLERD_RCCAL
alex_BLE_cy_m0s8_ble__READ_IQ_1 EQU CYREG_BLE_BLERD_READ_IQ_1
alex_BLE_cy_m0s8_ble__READ_IQ_2 EQU CYREG_BLE_BLERD_READ_IQ_2
alex_BLE_cy_m0s8_ble__READ_IQ_3 EQU CYREG_BLE_BLERD_READ_IQ_3
alex_BLE_cy_m0s8_ble__READ_IQ_4 EQU CYREG_BLE_BLERD_READ_IQ_4
alex_BLE_cy_m0s8_ble__RECEIVE_TRIG_CTRL EQU CYREG_BLE_BLELL_RECEIVE_TRIG_CTRL
alex_BLE_cy_m0s8_ble__RF_CONFIG EQU CYREG_BLE_BLESS_RF_CONFIG
alex_BLE_cy_m0s8_ble__RMAP EQU CYREG_BLE_BLERD_RMAP
alex_BLE_cy_m0s8_ble__RSSI EQU CYREG_BLE_BLERD_RSSI
alex_BLE_cy_m0s8_ble__RX EQU CYREG_BLE_BLERD_RX
alex_BLE_cy_m0s8_ble__RX_BUMP1 EQU CYREG_BLE_BLERD_RX_BUMP1
alex_BLE_cy_m0s8_ble__RX_BUMP2 EQU CYREG_BLE_BLERD_RX_BUMP2
alex_BLE_cy_m0s8_ble__SCAN_CONFIG EQU CYREG_BLE_BLELL_SCAN_CONFIG
alex_BLE_cy_m0s8_ble__SCAN_INTERVAL EQU CYREG_BLE_BLELL_SCAN_INTERVAL
alex_BLE_cy_m0s8_ble__SCAN_INTR EQU CYREG_BLE_BLELL_SCAN_INTR
alex_BLE_cy_m0s8_ble__SCAN_NEXT_INSTANT EQU CYREG_BLE_BLELL_SCAN_NEXT_INSTANT
alex_BLE_cy_m0s8_ble__SCAN_PARAM EQU CYREG_BLE_BLELL_SCAN_PARAM
alex_BLE_cy_m0s8_ble__SCAN_WINDOW EQU CYREG_BLE_BLELL_SCAN_WINDOW
alex_BLE_cy_m0s8_ble__SL_CONN_INTERVAL EQU CYREG_BLE_BLELL_SL_CONN_INTERVAL
alex_BLE_cy_m0s8_ble__SLAVE_LATENCY EQU CYREG_BLE_BLELL_SLAVE_LATENCY
alex_BLE_cy_m0s8_ble__SLAVE_TIMING_CONTROL EQU CYREG_BLE_BLELL_SLAVE_TIMING_CONTROL
alex_BLE_cy_m0s8_ble__SLV_WIN_ADJ EQU CYREG_BLE_BLELL_SLV_WIN_ADJ
alex_BLE_cy_m0s8_ble__SUP_TIMEOUT EQU CYREG_BLE_BLELL_SUP_TIMEOUT
alex_BLE_cy_m0s8_ble__SY EQU CYREG_BLE_BLERD_SY
alex_BLE_cy_m0s8_ble__SY_BUMP1 EQU CYREG_BLE_BLERD_SY_BUMP1
alex_BLE_cy_m0s8_ble__SY_BUMP2 EQU CYREG_BLE_BLERD_SY_BUMP2
alex_BLE_cy_m0s8_ble__TEST EQU CYREG_BLE_BLERD_TEST
alex_BLE_cy_m0s8_ble__TEST2_SY EQU CYREG_BLE_BLERD_TEST2_SY
alex_BLE_cy_m0s8_ble__THRSHD1 EQU CYREG_BLE_BLERD_THRSHD1
alex_BLE_cy_m0s8_ble__THRSHD2 EQU CYREG_BLE_BLERD_THRSHD2
alex_BLE_cy_m0s8_ble__THRSHD3 EQU CYREG_BLE_BLERD_THRSHD3
alex_BLE_cy_m0s8_ble__THRSHD4 EQU CYREG_BLE_BLERD_THRSHD4
alex_BLE_cy_m0s8_ble__THRSHD5 EQU CYREG_BLE_BLERD_THRSHD5
alex_BLE_cy_m0s8_ble__TIM_COUNTER_L EQU CYREG_BLE_BLELL_TIM_COUNTER_L
alex_BLE_cy_m0s8_ble__TRANSMIT_WINDOW_OFFSET EQU CYREG_BLE_BLELL_TRANSMIT_WINDOW_OFFSET
alex_BLE_cy_m0s8_ble__TRANSMIT_WINDOW_SIZE EQU CYREG_BLE_BLELL_TRANSMIT_WINDOW_SIZE
alex_BLE_cy_m0s8_ble__TX EQU CYREG_BLE_BLERD_TX
alex_BLE_cy_m0s8_ble__TX_BUMP1 EQU CYREG_BLE_BLERD_TX_BUMP1
alex_BLE_cy_m0s8_ble__TX_BUMP2 EQU CYREG_BLE_BLERD_TX_BUMP2
alex_BLE_cy_m0s8_ble__TX_EN_EXT_DELAY EQU CYREG_BLE_BLELL_TX_EN_EXT_DELAY
alex_BLE_cy_m0s8_ble__TX_RX_ON_DELAY EQU CYREG_BLE_BLELL_TX_RX_ON_DELAY
alex_BLE_cy_m0s8_ble__TX_RX_SYNTH_DELAY EQU CYREG_BLE_BLELL_TX_RX_SYNTH_DELAY
alex_BLE_cy_m0s8_ble__TXRX_HOP EQU CYREG_BLE_BLELL_TXRX_HOP
alex_BLE_cy_m0s8_ble__WAKEUP_CONFIG EQU CYREG_BLE_BLELL_WAKEUP_CONFIG
alex_BLE_cy_m0s8_ble__WAKEUP_CONTROL EQU CYREG_BLE_BLELL_WAKEUP_CONTROL
alex_BLE_cy_m0s8_ble__WCO_CONFIG EQU CYREG_BLE_BLESS_WCO_CONFIG
alex_BLE_cy_m0s8_ble__WCO_STATUS EQU CYREG_BLE_BLESS_WCO_STATUS
alex_BLE_cy_m0s8_ble__WCO_TRIM EQU CYREG_BLE_BLESS_WCO_TRIM
alex_BLE_cy_m0s8_ble__WHITELIST_BASE_ADDR EQU CYREG_BLE_BLELL_WHITELIST_BASE_ADDR
alex_BLE_cy_m0s8_ble__WIN_MIN_STEP_SIZE EQU CYREG_BLE_BLELL_WIN_MIN_STEP_SIZE
alex_BLE_cy_m0s8_ble__WINDOW_WIDEN_INTVL EQU CYREG_BLE_BLELL_WINDOW_WIDEN_INTVL
alex_BLE_cy_m0s8_ble__WINDOW_WIDEN_WINOFF EQU CYREG_BLE_BLELL_WINDOW_WIDEN_WINOFF
alex_BLE_cy_m0s8_ble__WL_ADDR_TYPE EQU CYREG_BLE_BLELL_WL_ADDR_TYPE
alex_BLE_cy_m0s8_ble__WL_ENABLE EQU CYREG_BLE_BLELL_WL_ENABLE
alex_BLE_cy_m0s8_ble__XTAL_CLK_DIV_CONFIG EQU CYREG_BLE_BLESS_XTAL_CLK_DIV_CONFIG

; motor_switch
motor_switch__0__DR EQU CYREG_GPIO_PRT3_DR
motor_switch__0__DR_CLR EQU CYREG_GPIO_PRT3_DR_CLR
motor_switch__0__DR_INV EQU CYREG_GPIO_PRT3_DR_INV
motor_switch__0__DR_SET EQU CYREG_GPIO_PRT3_DR_SET
motor_switch__0__HSIOM EQU CYREG_HSIOM_PORT_SEL3
motor_switch__0__HSIOM_MASK EQU 0x0000000F
motor_switch__0__HSIOM_SHIFT EQU 0
motor_switch__0__INTCFG EQU CYREG_GPIO_PRT3_INTR_CFG
motor_switch__0__INTR EQU CYREG_GPIO_PRT3_INTR
motor_switch__0__INTR_CFG EQU CYREG_GPIO_PRT3_INTR_CFG
motor_switch__0__INTSTAT EQU CYREG_GPIO_PRT3_INTR
motor_switch__0__MASK EQU 0x01
motor_switch__0__PA__CFG0 EQU CYREG_UDB_PA3_CFG0
motor_switch__0__PA__CFG1 EQU CYREG_UDB_PA3_CFG1
motor_switch__0__PA__CFG10 EQU CYREG_UDB_PA3_CFG10
motor_switch__0__PA__CFG11 EQU CYREG_UDB_PA3_CFG11
motor_switch__0__PA__CFG12 EQU CYREG_UDB_PA3_CFG12
motor_switch__0__PA__CFG13 EQU CYREG_UDB_PA3_CFG13
motor_switch__0__PA__CFG14 EQU CYREG_UDB_PA3_CFG14
motor_switch__0__PA__CFG2 EQU CYREG_UDB_PA3_CFG2
motor_switch__0__PA__CFG3 EQU CYREG_UDB_PA3_CFG3
motor_switch__0__PA__CFG4 EQU CYREG_UDB_PA3_CFG4
motor_switch__0__PA__CFG5 EQU CYREG_UDB_PA3_CFG5
motor_switch__0__PA__CFG6 EQU CYREG_UDB_PA3_CFG6
motor_switch__0__PA__CFG7 EQU CYREG_UDB_PA3_CFG7
motor_switch__0__PA__CFG8 EQU CYREG_UDB_PA3_CFG8
motor_switch__0__PA__CFG9 EQU CYREG_UDB_PA3_CFG9
motor_switch__0__PC EQU CYREG_GPIO_PRT3_PC
motor_switch__0__PC2 EQU CYREG_GPIO_PRT3_PC2
motor_switch__0__PORT EQU 3
motor_switch__0__PS EQU CYREG_GPIO_PRT3_PS
motor_switch__0__SHIFT EQU 0
motor_switch__DR EQU CYREG_GPIO_PRT3_DR
motor_switch__DR_CLR EQU CYREG_GPIO_PRT3_DR_CLR
motor_switch__DR_INV EQU CYREG_GPIO_PRT3_DR_INV
motor_switch__DR_SET EQU CYREG_GPIO_PRT3_DR_SET
motor_switch__INTCFG EQU CYREG_GPIO_PRT3_INTR_CFG
motor_switch__INTR EQU CYREG_GPIO_PRT3_INTR
motor_switch__INTR_CFG EQU CYREG_GPIO_PRT3_INTR_CFG
motor_switch__INTSTAT EQU CYREG_GPIO_PRT3_INTR
motor_switch__MASK EQU 0x01
motor_switch__PA__CFG0 EQU CYREG_UDB_PA3_CFG0
motor_switch__PA__CFG1 EQU CYREG_UDB_PA3_CFG1
motor_switch__PA__CFG10 EQU CYREG_UDB_PA3_CFG10
motor_switch__PA__CFG11 EQU CYREG_UDB_PA3_CFG11
motor_switch__PA__CFG12 EQU CYREG_UDB_PA3_CFG12
motor_switch__PA__CFG13 EQU CYREG_UDB_PA3_CFG13
motor_switch__PA__CFG14 EQU CYREG_UDB_PA3_CFG14
motor_switch__PA__CFG2 EQU CYREG_UDB_PA3_CFG2
motor_switch__PA__CFG3 EQU CYREG_UDB_PA3_CFG3
motor_switch__PA__CFG4 EQU CYREG_UDB_PA3_CFG4
motor_switch__PA__CFG5 EQU CYREG_UDB_PA3_CFG5
motor_switch__PA__CFG6 EQU CYREG_UDB_PA3_CFG6
motor_switch__PA__CFG7 EQU CYREG_UDB_PA3_CFG7
motor_switch__PA__CFG8 EQU CYREG_UDB_PA3_CFG8
motor_switch__PA__CFG9 EQU CYREG_UDB_PA3_CFG9
motor_switch__PC EQU CYREG_GPIO_PRT3_PC
motor_switch__PC2 EQU CYREG_GPIO_PRT3_PC2
motor_switch__PORT EQU 3
motor_switch__PS EQU CYREG_GPIO_PRT3_PS
motor_switch__SHIFT EQU 0

; Miscellaneous
CYDEV_BCLK__HFCLK__HZ EQU 48000000
CYDEV_BCLK__HFCLK__KHZ EQU 48000
CYDEV_BCLK__HFCLK__MHZ EQU 48
CYDEV_BCLK__SYSCLK__HZ EQU 48000000
CYDEV_BCLK__SYSCLK__KHZ EQU 48000
CYDEV_BCLK__SYSCLK__MHZ EQU 48
CYDEV_CHIP_DIE_LEOPARD EQU 1
CYDEV_CHIP_DIE_PANTHER EQU 18
CYDEV_CHIP_DIE_PSOC4A EQU 10
CYDEV_CHIP_DIE_PSOC5LP EQU 17
CYDEV_CHIP_DIE_TMA4 EQU 2
CYDEV_CHIP_DIE_UNKNOWN EQU 0
CYDEV_CHIP_FAMILY_PSOC3 EQU 1
CYDEV_CHIP_FAMILY_PSOC4 EQU 2
CYDEV_CHIP_FAMILY_PSOC5 EQU 3
CYDEV_CHIP_FAMILY_UNKNOWN EQU 0
CYDEV_CHIP_FAMILY_USED EQU CYDEV_CHIP_FAMILY_PSOC4
CYDEV_CHIP_JTAG_ID EQU 0x0E34119E
CYDEV_CHIP_MEMBER_3A EQU 1
CYDEV_CHIP_MEMBER_4A EQU 10
CYDEV_CHIP_MEMBER_4C EQU 15
CYDEV_CHIP_MEMBER_4D EQU 6
CYDEV_CHIP_MEMBER_4E EQU 4
CYDEV_CHIP_MEMBER_4F EQU 11
CYDEV_CHIP_MEMBER_4G EQU 2
CYDEV_CHIP_MEMBER_4H EQU 9
CYDEV_CHIP_MEMBER_4I EQU 14
CYDEV_CHIP_MEMBER_4J EQU 7
CYDEV_CHIP_MEMBER_4K EQU 8
CYDEV_CHIP_MEMBER_4L EQU 13
CYDEV_CHIP_MEMBER_4M EQU 12
CYDEV_CHIP_MEMBER_4N EQU 5
CYDEV_CHIP_MEMBER_4U EQU 3
CYDEV_CHIP_MEMBER_5A EQU 17
CYDEV_CHIP_MEMBER_5B EQU 16
CYDEV_CHIP_MEMBER_UNKNOWN EQU 0
CYDEV_CHIP_MEMBER_USED EQU CYDEV_CHIP_MEMBER_4F
CYDEV_CHIP_DIE_EXPECT EQU CYDEV_CHIP_MEMBER_USED
CYDEV_CHIP_DIE_ACTUAL EQU CYDEV_CHIP_DIE_EXPECT
CYDEV_CHIP_REV_LEOPARD_ES1 EQU 0
CYDEV_CHIP_REV_LEOPARD_ES2 EQU 1
CYDEV_CHIP_REV_LEOPARD_ES3 EQU 3
CYDEV_CHIP_REV_LEOPARD_PRODUCTION EQU 3
CYDEV_CHIP_REV_PANTHER_ES0 EQU 0
CYDEV_CHIP_REV_PANTHER_ES1 EQU 1
CYDEV_CHIP_REV_PANTHER_PRODUCTION EQU 1
CYDEV_CHIP_REV_PSOC4A_ES0 EQU 17
CYDEV_CHIP_REV_PSOC4A_PRODUCTION EQU 17
CYDEV_CHIP_REV_PSOC5LP_ES0 EQU 0
CYDEV_CHIP_REV_PSOC5LP_PRODUCTION EQU 0
CYDEV_CHIP_REV_TMA4_ES EQU 17
CYDEV_CHIP_REV_TMA4_ES2 EQU 33
CYDEV_CHIP_REV_TMA4_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_3A_ES1 EQU 0
CYDEV_CHIP_REVISION_3A_ES2 EQU 1
CYDEV_CHIP_REVISION_3A_ES3 EQU 3
CYDEV_CHIP_REVISION_3A_PRODUCTION EQU 3
CYDEV_CHIP_REVISION_4A_ES0 EQU 17
CYDEV_CHIP_REVISION_4A_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_4C_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4D_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4E_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION_256DMA EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION_256K EQU 0
CYDEV_CHIP_REVISION_4G_ES EQU 17
CYDEV_CHIP_REVISION_4G_ES2 EQU 33
CYDEV_CHIP_REVISION_4G_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_4H_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4I_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4J_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4K_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4L_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4M_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4N_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4U_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_5A_ES0 EQU 0
CYDEV_CHIP_REVISION_5A_ES1 EQU 1
CYDEV_CHIP_REVISION_5A_PRODUCTION EQU 1
CYDEV_CHIP_REVISION_5B_ES0 EQU 0
CYDEV_CHIP_REVISION_5B_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_USED EQU CYDEV_CHIP_REVISION_4F_PRODUCTION
CYDEV_CHIP_REV_EXPECT EQU CYDEV_CHIP_REVISION_USED
CYDEV_CONFIG_READ_ACCELERATOR EQU 1
CYDEV_CONFIG_UNUSED_IO_AllowButWarn EQU 0
CYDEV_CONFIG_UNUSED_IO EQU CYDEV_CONFIG_UNUSED_IO_AllowButWarn
CYDEV_CONFIG_UNUSED_IO_AllowWithInfo EQU 1
CYDEV_CONFIG_UNUSED_IO_Disallowed EQU 2
CYDEV_CONFIGURATION_COMPRESSED EQU 1
CYDEV_CONFIGURATION_MODE_COMPRESSED EQU 0
CYDEV_CONFIGURATION_MODE EQU CYDEV_CONFIGURATION_MODE_COMPRESSED
CYDEV_CONFIGURATION_MODE_DMA EQU 2
CYDEV_CONFIGURATION_MODE_UNCOMPRESSED EQU 1
CYDEV_DEBUG_PROTECT_KILL EQU 4
CYDEV_DEBUG_PROTECT_OPEN EQU 1
CYDEV_DEBUG_PROTECT EQU CYDEV_DEBUG_PROTECT_OPEN
CYDEV_DEBUG_PROTECT_PROTECTED EQU 2
CYDEV_DEBUGGING_DPS_Disable EQU 3
CYDEV_DEBUGGING_DPS_SWD EQU 2
CYDEV_DEBUGGING_DPS EQU CYDEV_DEBUGGING_DPS_SWD
CYDEV_DEBUGGING_ENABLE EQU 1
CYDEV_DFT_SELECT_CLK0 EQU 10
CYDEV_DFT_SELECT_CLK1 EQU 11
CYDEV_HEAP_SIZE EQU 0x80
CYDEV_IMO_TRIMMED_BY_USB EQU 0
CYDEV_IMO_TRIMMED_BY_WCO EQU 0
CYDEV_IS_EXPORTING_CODE EQU 0
CYDEV_IS_IMPORTING_CODE EQU 0
CYDEV_PROJ_TYPE EQU 0
CYDEV_PROJ_TYPE_BOOTLOADER EQU 1
CYDEV_PROJ_TYPE_LAUNCHER EQU 5
CYDEV_PROJ_TYPE_LOADABLE EQU 2
CYDEV_PROJ_TYPE_LOADABLEANDBOOTLOADER EQU 4
CYDEV_PROJ_TYPE_MULTIAPPBOOTLOADER EQU 3
CYDEV_PROJ_TYPE_STANDARD EQU 0
CYDEV_STACK_SIZE EQU 0x0800
CYDEV_USE_BUNDLED_CMSIS EQU 1
CYDEV_VARIABLE_VDDA EQU 1
CYDEV_VDDA_MV EQU 3300
CYDEV_VDDD_MV EQU 3300
CYDEV_VDDR_MV EQU 3300
CYDEV_WDT_GENERATE_ISR EQU 1
CYIPBLOCK_m0s8bless_VERSION EQU 1
CYIPBLOCK_m0s8cpussv2_VERSION EQU 1
CYIPBLOCK_m0s8csd_VERSION EQU 1
CYIPBLOCK_m0s8ioss_VERSION EQU 1
CYIPBLOCK_m0s8lcd_VERSION EQU 2
CYIPBLOCK_m0s8lpcomp_VERSION EQU 2
CYIPBLOCK_m0s8peri_VERSION EQU 1
CYIPBLOCK_m0s8scb_VERSION EQU 2
CYIPBLOCK_m0s8srssv2_VERSION EQU 1
CYIPBLOCK_m0s8tcpwm_VERSION EQU 2
CYIPBLOCK_m0s8udbif_VERSION EQU 1
CYIPBLOCK_s8pass4al_VERSION EQU 1
CYDEV_BOOTLOADER_ENABLE EQU 0
    ENDIF
    END
