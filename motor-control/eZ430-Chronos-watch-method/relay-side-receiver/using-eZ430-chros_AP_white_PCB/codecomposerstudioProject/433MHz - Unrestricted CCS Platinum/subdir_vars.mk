################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5509.cmd \
../msp430USB.cmd 

C_SRCS += \
../ccSPI.c \
../cmdHandler.c \
../main.c \
../usbConstructs.c \
../usbEventHandling.c 

OBJS += \
./ccSPI.obj \
./cmdHandler.obj \
./main.obj \
./usbConstructs.obj \
./usbEventHandling.obj 

C_DEPS += \
./ccSPI.pp \
./cmdHandler.pp \
./main.pp \
./usbConstructs.pp \
./usbEventHandling.pp 

C_DEPS__QUOTED += \
"ccSPI.pp" \
"cmdHandler.pp" \
"main.pp" \
"usbConstructs.pp" \
"usbEventHandling.pp" 

OBJS__QUOTED += \
"ccSPI.obj" \
"cmdHandler.obj" \
"main.obj" \
"usbConstructs.obj" \
"usbEventHandling.obj" 

C_SRCS__QUOTED += \
"../ccSPI.c" \
"../cmdHandler.c" \
"../main.c" \
"../usbConstructs.c" \
"../usbEventHandling.c" 


